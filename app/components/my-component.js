import Component from '@ember/component';

export default Component.extend({
    router:Ember. inject.service(),
    actions: {
        azureReportsClick() {
            this.get("router").transitionTo( "report.azure.reports",1,1,20 );
        },
        auditAzureReportsClick() {
            this.get("router").transitionTo("scope-report.azure.reports", "O365","scopeId","scopeData",12,1,2);
        },
        reportBoardClick() {
            this.get("router").transitionTo("scope-audit-report.azure.reports", "O365","scopeId","scopeData",12,12,12);
        },
        reportBoardReportsClick() {
            this.get("router").transitionTo("reportboard-report.azure.reports","O365","scopeId","scopeData",15,11,34,1,20);
        },
    }
});
