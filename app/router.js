import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});
function reportingRoutes() {

  this.route('report', { path: '/', resetNamespace: true }, function () {
    this.route('dashboards', { path: '/' }, function () {
      this.route('overall', { path: '/' });
    });
    this.route('azure', function () {
      this.route('reports', { path: '/reports/:reportId/:pageNumber/:pageSize' });
      this.route('dashboards', function () {
        this.route('security');
      });
    });
  });

}
function auditingRoutes() {

  this.route('audit', { resetNamespace: true }, function () {
    this.route('azure', function () {
      this.route('reports', { path: '/reports/:reportId/:pageNumber/:pageSize' });
    });
  });

}
Router.map(function () {
  reportingRoutes.call(this);
  auditingRoutes.call(this);

  this.route('product', { path: '/:name' }, function () {

    this.route('scope', { path: '/:scopeId/:scopedata', resetNamespace: true }, function () {
      
       reportingRoutes.call(this);
       auditingRoutes.call(this);
       this.alias('scope-report', '/', 'report');
       this.alias('scope-audit-report', '/audit', 'audit');

      this.route('reportboard', { resetNamespace: true }, function () {
        this.route('category', { path: '/:boardId' }, function () {
          this.route('categoryItems', { path: '/:categoryId' }, function () {
             reportingRoutes.call(this);
             auditingRoutes.call(this);
             this.alias('reportboard-report', '/', 'report');
             this.alias('reportboard-audit-report', '/audit', 'audit');

          });
        });
      });
    });
  });
});

export default Router;
