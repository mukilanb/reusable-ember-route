import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | report/azure/dashboards/security', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:report/azure/dashboards/security');
    assert.ok(route);
  });
});
