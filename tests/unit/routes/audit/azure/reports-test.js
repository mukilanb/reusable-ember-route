import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | audit/azure/reports', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:audit/azure/reports');
    assert.ok(route);
  });
});
