import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | reportboard/category', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:reportboard/category');
    assert.ok(route);
  });
});
