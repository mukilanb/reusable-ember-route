import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | reportboard/category/categoryItems', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:reportboard/category/category-items');
    assert.ok(route);
  });
});
